import 'dart:async';
import 'package:flutter/material.dart';
import 'login.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Timer(
        Duration(seconds: 3),
        () => Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (BuildContext context) => Login())));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Stack(
          children: <Widget>[
            new Center(
              child: Image.asset(
                'images/splashscreen.jpg',
                // width: 490.0,
                fit: BoxFit.fill,
                // height: 1200.0
              ),
            ),
            new Container(
              alignment: Alignment.topCenter,
              margin: const EdgeInsets.fromLTRB(0.0, 60.0, 0.0, 0.0),
              child: new Text(
                "Welcome to My Clinic",
                style: new TextStyle(
                    color: Colors.orangeAccent,
                    fontSize: 30.0,
                    fontWeight: FontWeight.w500),
              ),
            ),
            new Container(
              alignment: Alignment.bottomCenter,
              margin: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 20.0),
              child: new Text(
                "Please wait...",
                style:
                    new TextStyle(fontSize: 30.0, fontWeight: FontWeight.w100),
              ),
            ),
          ],
        ));
  }
}
