import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:my_clinic/patient.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'doctor.dart';
import 'loading.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn _googleSignIn = new GoogleSignIn();
  SharedPreferences prefs;
  bool isLoggedIn = false;
  FirebaseUser currentUser;

  bool showLoader = false;

  String doctorEmail = "mubin.khalife@gmail.com";

  @override
  void initState() {
    super.initState();
    checkLogin();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text("Login"),
        centerTitle: true,
      ),
      body: Stack(
        children: <Widget>[
          Center(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: FlatButton(
                  color: Colors.redAccent,
                  onPressed: _gSignIn,
                  child: new Text(
                    "Login with Google",
                    style: new TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 18.0),
                  )),
            ),
          ),
          Positioned(
            child: showLoader ? Loading() : Container(),
          )
        ],
      ),
    );
  }

  Future<Null> _gSignIn() async {
    GoogleSignInAccount googleSignInAccount = await _googleSignIn.signIn();
    GoogleSignInAuthentication googleSignInAuthentication =
        await googleSignInAccount.authentication;

    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );

    FirebaseUser user = (await _auth.signInWithCredential(credential)).user;

    if (user != null) {
      this.setState(() {
        showLoader = false;
      });

      final QuerySnapshot result = await Firestore.instance
          .collection('users')
          .where('id', isEqualTo: user.uid)
          .getDocuments();

      final List<DocumentSnapshot> documents = result.documents;

      bool isDoctor = false;

      //Check if the logged in user is Doctor
      if (user.email == doctorEmail) {
        isDoctor = true;
        await prefs.setBool('isDoctor', true);
      } else {
        await prefs.setBool('isDoctor', false);
      }

      if (documents.length == 0) {
        // Update data to server if new user
        Firestore.instance.collection('users').document(user.uid).setData({
          'nickname': user.displayName,
          'photoUrl': user.photoUrl,
          'id': user.uid,
          'createdAt': DateTime.now().millisecondsSinceEpoch.toString(),
          'chattingWith': null,
          'isDoctor': isDoctor,
          'email': user.email
        });

        // Write data to local
        currentUser = user;
        await prefs.setString('id', currentUser.uid);
        await prefs.setString('nickname', currentUser.displayName);
        await prefs.setString('photoUrl', currentUser.photoUrl);
      } else {
        // Write data to local
        await prefs.setString('id', documents[0]['id']);
        await prefs.setString('nickname', documents[0]['nickname']);
        await prefs.setString('photoUrl', documents[0]['photoUrl']);
      }

      Fluttertoast.showToast(msg: "Sign in success");

      //If doctor, got to Doctor's home screen
      if (isDoctor) {
        this.setState(() {
          showLoader = false;
        });
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => Doctor(currentUserId: user.uid)));
      } else {
        //The logged in user is a Patient. Go to directly to chat screen
        //get doctor's user id
        final QuerySnapshot result = await Firestore.instance
            .collection('users')
            .where('email', isEqualTo: doctorEmail)
            .getDocuments();

        final doctorUserId = result.documents[0]["id"];
        final doctorUserAvatar = result.documents[0]["photoUrl"];
        final doctorUserName = result.documents[0]["nickname"];

        this.setState(() {
          showLoader = false;
        });

        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => Patient(
                    currentUserId: user.uid,
                    doctorUserId: doctorUserId,
                    doctorUserAvatar: doctorUserAvatar,
                    doctorUserName: doctorUserName)));
      }
    } else {
      Fluttertoast.showToast(msg: "Sign-in failed");
      this.setState(() {
        showLoader = false;
      });
    }
  }

  void checkLogin() async {
    this.setState(() {
      showLoader = true;
    });

    prefs = await SharedPreferences.getInstance();

    isLoggedIn = await _googleSignIn.isSignedIn();

    if (isLoggedIn) {
      if (prefs.getBool("isDoctor")) {
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  Doctor(currentUserId: prefs.getString('id'))),
        );
      } else {
        final QuerySnapshot result = await Firestore.instance
            .collection('users')
            .where('email', isEqualTo: doctorEmail)
            .getDocuments();

        final doctorUserId = result.documents[0]["id"];
        final doctorUserAvatar = result.documents[0]["photoUrl"];
        final doctorUserName = result.documents[0]["nickname"];

        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => Patient(
                    currentUserId: prefs.getString('id'),
                    doctorUserId: doctorUserId,
                    doctorUserAvatar: doctorUserAvatar,
                    doctorUserName: doctorUserName)));
      }
    }

    this.setState(() {
      showLoader = false;
    });
  }
}
