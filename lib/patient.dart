import 'dart:convert';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_sign_in/google_sign_in.dart';

import 'chat.dart';

class Patient extends StatefulWidget {
  final String currentUserId;
  final String doctorUserId;
  final String doctorUserAvatar;
  final String doctorUserName;

  Patient(
      {Key key,
      @required this.currentUserId,
      @required this.doctorUserId,
      @required this.doctorUserAvatar,
      @required this.doctorUserName})
      : super(key: key);

  @override
  _PatientState createState() => _PatientState(
      currentUserId: currentUserId,
      doctorUserId: doctorUserId,
      doctorUserAvatar: doctorUserAvatar,
      doctorUserName: doctorUserName);
}

class _PatientState extends State<Patient> {
  _PatientState(
      {Key key,
      @required this.currentUserId,
      @required this.doctorUserId,
      @required this.doctorUserAvatar,
      @required this.doctorUserName});

  final String currentUserId;
  final String doctorUserId;
  final String doctorUserAvatar;
  final String doctorUserName;

  final FirebaseMessaging firebaseMessaging = FirebaseMessaging();
  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  final GoogleSignIn googleSignIn = GoogleSignIn();

  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    registerNotification();
    configLocalNotification();
    // rediretToChat();
  }

  void registerNotification() {
    firebaseMessaging.requestNotificationPermissions();

    firebaseMessaging.configure(onMessage: (Map<String, dynamic> message) {
      print('onMessage: $message');
      Platform.isAndroid
          ? showNotification(message['notification'])
          : showNotification(message['aps']['alert']);
      return;
    }, onResume: (Map<String, dynamic> message) {
      print('onResume: $message');
      return;
    }, onLaunch: (Map<String, dynamic> message) {
      print('onLaunch: $message');
      return;
    });

    firebaseMessaging.getToken().then((token) {
      print('token: $token');
      Firestore.instance
          .collection('users')
          .document(currentUserId)
          .updateData({'pushToken': token});
    }).catchError((err) {
      Fluttertoast.showToast(msg: err.message.toString());
    });
  }

  void configLocalNotification() {
    var initializationSettingsAndroid =
        new AndroidInitializationSettings('@mipmap/ic_launcher');
    var initializationSettingsIOS = new IOSInitializationSettings();
    var initializationSettings = new InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings);
  }

  void showNotification(message) async {
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
      'com.example.my_clinic',
      'Doctor Patient Confidentiality',
      'My Clinic',
      playSound: true,
      enableVibration: true,
      importance: Importance.Max,
      priority: Priority.High,
    );
    var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
    var platformChannelSpecifics = new NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);

    print(message);

    await flutterLocalNotificationsPlugin.show(0, message['title'].toString(),
        message['body'].toString(), platformChannelSpecifics,
        payload: json.encode(message));
  }

  // void rediretToChat() {
  //   Navigator.push(
  //       context,
  //       MaterialPageRoute(
  //           builder: (context) => Chat(
  //                 userId: doctorUserId,
  //                 userAvatar: doctorUserAvatar,
  //                 userName: doctorUserName,
  //               )));
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text("Welcome"),
        centerTitle: true,
      ),
      body: Stack(
        children: <Widget>[
          Center(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: FlatButton(
                  color: Colors.blueAccent,
                  // onPressed: () {debugPrint("clicked");},
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => Chat(
                                  userId: doctorUserId,
                                  userAvatar: doctorUserAvatar,
                                  userName: doctorUserName,
                                )));
                  },
                  child: new Text(
                    "Chat with Doctor",
                    style: new TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 18.0),
                  )),
            ),
          ),
        ],
      ),
    );
  }
}
