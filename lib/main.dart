import 'package:flutter/material.dart';
import 'package:my_clinic/splashscreen.dart';

void main(){
  runApp(new MaterialApp(
    title: "",
    debugShowCheckedModeBanner: false,
    home: new SplashScreen(),
  ));
}